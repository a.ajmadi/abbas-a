const fs = require("fs");
const express = require("express");
const app = express();
const PORT = process.env.PORT || 7006; // PORT als umgebungsvariable
const server = app.listen(PORT, () => {
  console.log(`Express server auf ${PORT} gestartet`);
});

app.use((req, res, next) => {
  console.log("request");
  res.set({
    "Acces-control-Allow-Origin": "*",
    "Acces-control-Allow-Origin": "GET,POST",
    "Acces-control-Allow-Origin":
      "Origin, x-requested-with, Content-type, Accept",
  });
  next();
});

app.use(express.urlencoded({ extended: false })); // POST Request mit Daten annehmen <- request.body
app.use(express.json()); // wenn Daten als JSON geschickt werden (vom Client an den Server)

//
const pas = require("passport"); // npm i passport
const Local = require("passport-local"); // npm i passport-local
// Session => merkt user user damit ich später abfragen ob usr eingelogt ist oder nicht
const session = require("express-session"); // npm i express-session
// crypto => zum verschlussel
const crypto = require("crypto"); // macht passwort nicht klartext  // npm i crypto
// const { json } = require('express');

// hilfs funktion das password nicht in klartext zu speichern
const hashPass = (pwd) => {
  let salt = "Das ist ein geheimer Salt der ganze lange ist";
  let sicheresPwd = crypto
    .createHash("md5")
    .update(salt + pwd + salt)
    .digest("hex");
  return sicheresPwd;
};

// ueberpruefen ob User Eingelogt ist
const checkUserLoggedIn = (req, res, next) => {
  if (req.user) {
    next();
  } else {
    // res.send('Error, logen Sie sich bitte ein' );
    res.redirect("/login");
  }
};

// Middleware fur Passport und Local-Strategy
pas.serializeUser((user, done) => {
  // Hilfs Function Wandelt User in String um und wird weiter gearbeitet
  done(null, user);
});

pas.deserializeUser((user, done) => {
  // Wandelt zurück
  done(null, user);
});

// strategy
pas.use(
  new Local((username, password, done) => {
    // Check ob User ok ist oder nicht
    fs.readFile(__dirname + "/data/users.json", (err, data) => {
      let users = JSON.parse(data);
      let userFound = false;

      for (let u of users) {
        if (u.user == username && u.pwd == hashPass(password)) {
          userFound = true;
        }
      }

      if (userFound) {
        done(null, { username: username });
      } else {
        done(null); // nicts eingegeben, nicht eingelogt
      }
    });
  })
);
// Express server nutzt Session(module)
app.use(
  session({
    // app.use => ist middleware
    secret: "geheim",
    resave: false,
    saveUninitialized: false, // wird nichts gespeicher wenn nichts gibst
  })
);
app.use(pas.initialize());
app.use(pas.session());

app.post(
  "/login2",
  pas.authenticate("local", {
    failureRedirect: "/login/index.html?error",
    successRedirect: "/admin",
  }),
  (req, res) => {
    console.log(req.user);
    // res.send( 'Login ok ');
    res.redirect("/admin");
  }
);

app.use("/admin", checkUserLoggedIn);
app.get("/admin/geheim", (req, res) => {
  res.send("nur für eingelogte User sichtbar....");
});

app.get("/members/hello", (req, res) => {
  res.send("Hello" + req.user.username + "!");
});

app.get("/logout", (req, res) => {
  req.logOut("/logout"); // diese befehl kommt von passport ist kein standard befehl
  // res.send('ausgelogt.');
  res.redirect("/login");
});

app.post("/register", (req, res) => {
  let un = req.body.name;
  let pw = req.body.password;

  fs.readFile(__dirname + "/data/users.json", (err, data) => {
    console.log("sie sind registeried");
    let users = JSON.parse(data);
    users.push({
      user: un,
      pwd: hashPass(pw),
    });

    fs.writeFile(
      __dirname + "/data/users.json",
      JSON.stringify(users),
      (err) => {
        // res.send('user gespeichert');
        res.redirect("/login");
      }
    );
  });
});

app.post("/register1", (req, res) => {
  let un = req.body.name;
  let pw = req.body.password;

  fs.readFile(__dirname + "/data/users.json", (err, data) => {
    console.log("sie sind registeried");
    let users = JSON.parse(data);
    users.push({
      user: un,
      pwd: hashPass(pw),
    });

    fs.writeFile(
      __dirname + "/data/users.json",
      JSON.stringify(users),
      (err) => {
        // res.send('user gespeichert');
        res.redirect("/inedex.html");
      }
    );
  });
});

app.post(
  "/login1",
  pas.authenticate("local", {
    failureRedirect: "/index.html?error",
    successRedirect: "/user",
  }),
  (req, res) => {
    console.log(req.user);
    res.redirect("/user");
  }
);

app.use("/user", checkUserLoggedIn);
app.get("/members/geheim", (req, res) => {
  res.send("Logen Sie bitte ein...");
});

app.get("/members/hello", (req, res) => {
  res.send("Hello" + req.user.username + "!");
});

app.get("/logout1", (req, res) => {
  req.logOut("/login"); // diese befehl kommt von passport ist kein standard befehl
  res.redirect("/login");
});

app.use(express.static("www"));

let pfadJSON = __dirname + "/data/vokabel.json";

app.post("/speicherFrageAntwort", (request, response) => {
  let q = request.body.vokabelQ;
  let a1 = request.body.antwort1;
  let a2 = request.body.antwort2;
  let a3 = request.body.antwort3;
  let a4 = request.body.antwort4;
  let richtigeAntwort = request.body.richtigeAntwort;
  let kategorie = request.body.kategorie;

  fs.readFile(pfadJSON, (err, data) => {
    let voc = JSON.parse(data);

    if ("sport" === kategorie) {
      voc.sport.push({
        text: q,
        answers: [a1, a2, a3, a4],
        indexCorrect: richtigeAntwort,
      });
    }
    if ("geographie" === kategorie) {
      voc.geographie.push({
        text: q,
        answers: [a1, a2, a3, a4],
        indexCorrect: richtigeAntwort,
      });
    }
    fs.writeFile(pfadJSON, JSON.stringify(voc), () => {
      // JSON Datei wurde aktualisiert
      response.status(200).end("OK");
    });
  });
});

app.get("/ladeVokabel", (request, response) => {
  fs.readFile(pfadJSON, (err, data) => {
    let voc = JSON.parse(data);
    response.send(voc); // Inhalt der Datei wird als JSON direkt zuückgeschickt.
  });
});

app.get("/ladeFrageSport", (request, response) => {
  fs.readFile(pfadJSON, (err, data) => {
    let voc = JSON.parse(data);
    let zufall = Math.floor(Math.random() * voc.sport.length);
    response.send(voc.sport[zufall]); // Inhalt der Datei wird als JSON direkt zuückgeschickt.
  });
});

app.get("/ladeFrageGeographie", (request, response) => {
  fs.readFile(pfadJSON, (err, data) => {
    let voc = JSON.parse(data);
    let zufall = Math.floor(Math.random() * voc.geographie.length);
    response.send(voc.geographie[zufall]); // Inhalt der Datei wird als JSON direkt zuückgeschickt.
  });
});

app.post("/loeschen", (request, response) => {
  let id = request.body.vokabelID;
  let kategorie = request.body.kategorie;
  console.log(id);
  fs.readFile(pfadJSON, (err, data) => {
    let voc = JSON.parse(data);
    if ("sport" === kategorie) {
      voc.sport.splice(id, 1);
    }
    if ("geographie" === kategorie) {
      voc.geographie.splice(id, 1);
    }
    fs.writeFile(pfadJSON, JSON.stringify(voc), () => {
      response.status(200).end("OK");
    });
  });
});
