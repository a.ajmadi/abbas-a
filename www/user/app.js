const start = document.querySelector(".start");
const quiz = document.querySelector(".quiz");
const question = document.querySelector(".question");
const allAnswer = document.querySelectorAll(".answer");
const answerA = document.querySelector("#A");
const answerB = document.querySelector("#B");
const answerC = document.querySelector("#C");
const answerD = document.querySelector("#D");
const counter = document.querySelector(".counter");
const timeGauge = document.querySelector(".time-gauge");
////////

let correctAnswer;
const progressContainer = document.querySelector(".progress-container");
const ScoreContainer = document.querySelector(".score-container");

let Questions = [];
let kategorie = document.getElementById("hiddenKategorieValue");

// Variablen
const lastQuestion = 9; // wennever we r in the last question afther that we wanna show score container
let activeQuestion = 0; // is the question that the user activly answeer
const questionTime = 10; // 10 second
const gaugeWidth = 800; // 800px
const gaugeUnit = gaugeWidth / questionTime; // 80px
let count = 0;
let TIMER;
let score = 0;

// Start Button EventListener
start.addEventListener("click", startQuiz);

// Answer choices Event Listeners
allAnswer.forEach(function (clickAnswer) {
  clickAnswer.addEventListener("click", function (e) {
    let userAnswer = e.target.innerHTML.trim();
    checkAnswer(userAnswer);
  });
});

// renderQuestion Function     render show our question to the page
function renderQuestion() {
  // let q = Questions[activeQuestion];
  // console.log(q);
  // question.innerHTML = "<p>" + q.question + "</p>";
  // answerA.innerHTML = q.answerA;
  // answerB.innerHTML = q.answerB;
  // answerC.innerHTML = q.answerC;
  // answerD.innerHTML = q.answerD;
}

/////////////////////////////////
// Read
const readQuestionsFromVokabelJson = () => {
  const urlSearchParams = new URLSearchParams(window.location.search);
  const params = Object.fromEntries(urlSearchParams.entries());

  let endPoint;
  if ("sport" === params.kategorie) {
    endPoint = "/ladeFrageSport";
  }
  if ("geographie" === params.kategorie) {
    endPoint = "/ladeFrageGeographie";
  }
  fetch(endPoint, { method: "get" })
    .then((res) => res.json())
    .then((res) => {
      let data = res;
      question.innerHTML = "<p>" + data.text + "</p>";
      answerA.innerHTML = data.answers[0];
      answerB.innerHTML = data.answers[1];
      answerC.innerHTML = data.answers[2];
      answerD.innerHTML = data.answers[3];
      correctAnswer = data.answers[data.indexCorrect];
    });
};

//////////////////////////////////////////////
// startQuiz Function
function startQuiz() {
  start.style.display = "none";
  h1.style.display = "none";
  quiz.style.visibility = "visible"; // da machen wieder visible in CSS ist auf hidden
  readQuestionsFromVokabelJson();
  renderQuestion();
  renderProgress(); // ist die boxen mit richtig oder falshe farbe
  renderCounter(); ////  ??? ///////
  TIMER = setInterval(renderCounter, 1000);
}

////////////////////////////////////////////////////////
// Progress Function
function renderProgress() {
  for (let questionIndex = 0; questionIndex <= lastQuestion; questionIndex++) {
    progressContainer.innerHTML +=
      "<div class='progress-box' id=" + questionIndex + "></div>";
    // Da bauen wir 10 boxen für 10 frage wenn da mehr fragen kommt wird automatish mehr boxen gebaut
  }
}
////////////////////////////////////////////////////////
// Counter function
function renderCounter() {
  if (count <= questionTime) {
    counter.innerHTML = count;
    timeGauge.style.width = count * gaugeUnit + "px";
    count++;
  } else {
    count = 0;
    answerIsIncorrect();
    nextQuestion();
  }
}

////////////////////////////////////////////////////////
// checkAnswer Function
function checkAnswer(answer) {
  // console.log(correctAnswer,answer,answer.trim()== correctAnswer.trim());
  if (answer.trim() == correctAnswer.trim()) {
    console.log("richtig");
    score++;
    answerIsCorrect();
  } else {
    answerIsIncorrect();
  }
  nextQuestion();
}

////////////////////////////////////////////////////////
// answerIsCorrect Function
function answerIsCorrect() {
  document.getElementById(activeQuestion).style.backgroundColor = "green";
}

// answerIsIncorrect Function
function answerIsIncorrect() {
  document.getElementById(activeQuestion).style.backgroundColor = "red";
}

////////////////////////////////////////////////////////
// nextQuestion Function
function nextQuestion() {
  count = 0;
  if (activeQuestion < lastQuestion) {
    activeQuestion++;
    readQuestionsFromVokabelJson();
    // renderQuestion();
  } else {
    clearInterval(TIMER);
    renderScore();
  }
}

////////////////////////////////////////////////////////
// Score Function
function renderScore() {
  ScoreContainer.style.visibility = "visible";

  let scorePercentage = Math.round((100 * score) / 10);
  ScoreContainer.innerHTML = `<h2>Prozent der richtigen Antworten: ${scorePercentage}</h2>`;
  ScoreContainer.innerHTML += `<h2>Zahl der richtigen Antworten: ${score}</h2>`;
}
