document.getElementById("save").onclick = (e) => {
  e.preventDefault(); // damit wird <form> nicht submitted!!! nur AJAX-Request siehe unten

  let quest = document.getElementById("vokabelQ").value;
  let ans1 = document.getElementById("antwort1").value;
  let ans2 = document.getElementById("antwort2").value;
  let ans3 = document.getElementById("antwort3").value;
  let ans4 = document.getElementById("antwort4").value;
  let richtigeAntwort = document.getElementById("richtigeAntwort").value;
  let kategorie = document.getElementById("select").value;

  if (
    ans1 != "" &&
    ans2 != "" &&
    ans3 != "" &&
    ans4 != "" &&
    richtigeAntwort != "" &&
    quest != ""
  ) {
    fetch("/speicherFrageAntwort", {
      method: "post",
      headers: {
        "Content-Type":
          "application/x-www-form-urlencoded" /* fetch macht das nicht automatisch! */,
      },
      body:
        "vokabelQ=" +
        quest +
        "&antwort1=" +
        ans1 +
        "&antwort2=" +
        ans2 +
        "&antwort3=" +
        ans3 +
        "&antwort4=" +
        ans4 +
        "&richtigeAntwort=" +
        richtigeAntwort +
        "&kategorie=" +
        kategorie,
    })
      .then((response) => response.text()) // response.text()
      .then((data) => {
        erzeugeTabelle();
        document.querySelector("form").reset();
        // alert("Vokabel gespeichert!!!");
      });
  }
};

let loeschen = function (welche, kategorie) {
  fetch("/loeschen", {
    method: "post",
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
    },
    body: "vokabelID=" + welche + "&kategorie=" + kategorie,
  }).then((res) => {
    erzeugeTabelle();
    alert("Vokabel gelöscht!");
  });
};

let erzeugeTabelle = function () {
  fetch("/ladeVokabel", { method: "get" })
    .then((res) => res.json())
    .then((voc) => {
      console.log(voc);
      let sportZeilen = document.querySelectorAll(".sportTable tr");
      for (let i = 1; i < sportZeilen.length; i++) {
        document.getElementById("sportTable").removeChild(sportZeilen[i]);
      }
      let sport = voc.sport;
      console.log(sport);

      for (let i in sport) {
        let tr = document.createElement("tr");

        let td;
        td = document.createElement("td");
        td.innerHTML = sport[i].text;

        tr.appendChild(td);
        td = document.createElement("td");
        td.innerHTML = sport[i].answers[0];
        tr.appendChild(td);
        td = document.createElement("td");

        tr.appendChild(td);
        td.innerHTML = sport[i].answers[1];
        tr.appendChild(td);
        td = document.createElement("td");

        tr.appendChild(td);
        td.innerHTML = sport[i].answers[2];
        tr.appendChild(td);
        td = document.createElement("td");

        tr.appendChild(td);
        td.innerHTML = sport[i].answers[3];
        tr.appendChild(td);
        td = document.createElement("td");

        tr.appendChild(td);
        td.innerHTML = sport[i].indexCorrect;
        tr.appendChild(td);
        td = document.createElement("td");

        let hyperlink = document.createElement("a");
        hyperlink.href = "#";
        hyperlink.innerHTML = "löschen";
        td.appendChild(hyperlink);
        hyperlink.onclick = (e) => {
          loeschen(i, "sport");
        };
        tr.appendChild(td);

        document.getElementById("sportTable").appendChild(tr);
      }
      // Geographie

      let geographieZeilen = document.querySelectorAll(".geographieTable tr");
      for (let i = 1; i < geographieZeilen.length; i++) {
        document
          .getElementById("geographieTable")
          .removeChild(geographieZeilen[i]);
      }
      let geographie = voc.geographie;
      console.log(geographie);
      for (let i in geographie) {
        let tr = document.createElement("tr");
        document.getElementById("geographieTable").appendChild(tr);
        let td;
        td = document.createElement("td");
        td.innerHTML = geographie[i].text;

        tr.appendChild(td);
        td = document.createElement("td");
        td.innerHTML = geographie[i].answers[0];
        tr.appendChild(td);
        td = document.createElement("td");

        tr.appendChild(td);
        td.innerHTML = geographie[i].answers[1];
        tr.appendChild(td);
        td = document.createElement("td");

        tr.appendChild(td);
        td.innerHTML = geographie[i].answers[2];
        tr.appendChild(td);
        td = document.createElement("td");

        tr.appendChild(td);
        td.innerHTML = geographie[i].answers[3];
        tr.appendChild(td);
        td = document.createElement("td");

        tr.appendChild(td);
        td.innerHTML = geographie[i].indexCorrect;
        tr.appendChild(td);
        td = document.createElement("td");

        let hyperlink = document.createElement("a");
        hyperlink.href = "#";
        hyperlink.innerHTML = "löschen";
        td.appendChild(hyperlink);
        hyperlink.onclick = (e) => {
          loeschen(i, "geographie");
        };
        tr.appendChild(td);
      }
    });
};

erzeugeTabelle(); // wenn Seite geöffnet wird
